package com.bibao.boot.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.bibao.boot")
@EnableAspectJAutoProxy
public class AspectJConfig {

}
