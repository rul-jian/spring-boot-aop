package com.bibao.boot.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.config.AspectJConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AspectJConfig.class})
public class AspectJTest {
	@Autowired
	private MyService service;
	
	@Test
	public void testGetFullName() {
		assertNotSame(service.getClass(), MyService.class);
		String result = service.getFullName("Steven", "Zhang");
		assertEquals("Hello Bob Zhu, welcome to AspectJ!", result);
	}

	@Test
	public void testPrintArray() {
		int[] array = {5, 1, 3, 2, 4};
		assertEquals("[1,2,3,4,5]", service.printArray(array));
	}
	
	@Test
	public void testGenerateList() {
		List<String> list = service.generateList(new ArrayList<>());
		assertEquals(3, list.size());
		assertEquals("A", list.get(0));
		assertEquals("B", list.get(1));
		assertEquals("C", list.get(2));
	}
}
